/*
 *   Copyright (C) 2006 Aaron Seigo <aseigo@kde.org>
 *   Copyright (C) 2007 Riccardo Iaconelli <riccardo@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License version 2 as
 *   published by the Free Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef INTERFACE_H
#define INTERFACE_H

#include <QTimer>
#include <QGraphicsView>
#include <QGraphicsItem>

// pulls in definition for Window
#include <KSelectionWatcher>

// libplasma includes
#include <plasma/abstractrunner.h>
#include <plasma/svg.h>
#include <plasma/widgets/pushbutton.h>
#include <plasma/widgets/lineedit.h>
#include <plasma/widgets/layoutitem.h>
#define KRUNNER_SIZE 350, 101

class QLabel;
class QListWidget;
class QListWidgetItem;
class QVBoxLayout;

class KLineEdit;
class KPushButton;
class KTitleWidget;

class CollapsibleWidget;
class SearchMatch;

class PushIcon;
class GenericItem;

class Plasma::LineEdit;

class Interface : public QGraphicsView
{
    Q_OBJECT
    Q_CLASSINFO( "D-Bus Interface", "org.kde.krunner.Interface" )

    public:
        explicit Interface( QWidget* parent = 0 );
        ~Interface();

    public Q_SLOTS:
        // DBUS interface. if you change these methods, you MUST run:
        // qdbuscpp2xml interface.h -o org.kde.krunner.Interface.xml
        void display(const QString& term = QString());
        void switchUser();

    protected Q_SLOTS:
        void slotResize(int size);
        void match( const QString& term );
        void setWidgetPalettes();
        void updateMatches();
        void exec();
        void matchActivated( QListWidgetItem* );
        void fuzzySearch();
        void showOptions(bool show);
        void setDefaultItem( QListWidgetItem* );
        void expand();
        void collapse();

    protected:
        void showEvent( QShowEvent* e );
        void hideEvent( QHideEvent* e );
//         void paintEvent( QPaintEvent* e );
        void resizeEvent( QResizeEvent* e);

    private:
        void resetInterface();

        Plasma::Svg* m_background;
        bool m_expanded;

//         MainItem *item;

        QTimer m_searchTimer;
        Plasma::AbstractRunner::List m_runners;

//         QVBoxLayout* m_layout;
//         KTitleWidget* m_header;
        KLineEdit* m_searchTerm;
//         QListWidget* m_matchList;
//         QLabel* m_optionsLabel;
//         KPushButton* m_cancelButton;
//         KPushButton* m_runButton;
//         KPushButton* m_optionsButton;
//         CollapsibleWidget* m_expander;
        PushIcon *m_toggleExpand;
        PushIcon *close;
        GenericItem *topleft;
        GenericItem *top;
        GenericItem *topright;
        GenericItem *separator;
        GenericItem *separator2;
        GenericItem *background;
        GenericItem *bottomleft;
        GenericItem *bottom;
        GenericItem *bottomright;
        Plasma::LineEdit *lineEdit;


        SearchMatch* m_defaultMatch;
        QMap<Plasma::AbstractRunner*, SearchMatch*> m_matches;
        QList<SearchMatch*> m_searchMatches;
};

class GenericItem : public QGraphicsItem,
                    public QObject
//                     public Plasma::LayoutItem
{
//     Q_OBJECT

    public:
        GenericItem(Plasma::Svg *renderer, Interface *qgv);
        void paint(QPainter *painter,
                   const QStyleOptionGraphicsItem *option,
                   QWidget *widget);
        virtual QRectF boundingRect() const;
        void setElement(const QString &);
        void setSize(int w, int h) { m_w = w; m_h = h; }

    private:
        QRectF itemRectF() const;

        Plasma::Svg* m_background;
        bool m_expanded;
        QString m_element;
        Interface *m_qgv;
        int m_w, m_h;
};


// class BackgroundItem : public QGraphicsItem, public QObject
// {
// //     Q_OBJECT
//     public:
//         BackgroundItem(Plasma::Svg *renderer);
//         void paint(QPainter *painter,
//                    const QStyleOptionGraphicsItem *option,
//                    QWidget *widget);
//         virtual QRectF boundingRect() const;
//         void setType(const QString &type);
//         void setElement(
//     private:
//         Plasma::Svg* m_background;
//         bool m_expanded;
//         QString m_type;
// 
// };
// 
// class ForegroundItem : public QGraphicsItem, public QObject
// {
// //     Q_OBJECT
//     public:
//         BackgroundItem(Plasma::Svg *renderer);
//         void paint(QPainter *painter,
//                    const QStyleOptionGraphicsItem *option,
//                    QWidget *widget);
//         virtual QRectF boundingRect() const;
//     private:
//         Plasma::Svg *m_background;
//         bool m_expanded;
// 
// };
// 
class PushIcon : public Plasma::PushButton
{
    public:
        PushIcon(Interface *qgv) { m_qgv = qgv; }
        virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
        virtual QRectF boundingRect() const { return QRectF(0, 0, 22, 22); }
        void setIcon(const QString &icon);
    private:
        QString m_icon;
        Interface *m_qgv;
};

#endif
