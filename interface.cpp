/*
 *   Copyright (C) 2006 Aaron Seigo <aseigo@kde.org>
 *   Copyright (C) 2007 Riccardo Iaconelli <riccardo@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License version 2 as
 *   published by the Free Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QAction>
#include <QApplication>
#include <QLabel>
#include <QListWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QShortcut>
#include <QTimer>
#include <QHideEvent>
#include <QGraphicsItem>
#include <QTimeLine>

#include <KActionCollection>
#include <KDebug>
#include <KDialog>
#include <KLineEdit>
#include <KLocale>
#include <KGlobalSettings>
#include <KPushButton>
#include <KStandardGuiItem>
#include <KTitleWidget>
#include <KWindowSystem>

#include <plasma/abstractrunner.h>
#include <QPainter>
#include <QSvgRenderer>
#include <QResizeEvent>
#include <QBitmap>

#include <KDebug>

#include <plasma/widgets/lineedit.h>

#include "runners/services/servicerunner.h"
#include "runners/sessions/sessionrunner.h"
#include "runners/shell/shellrunner.h"
#include "collapsiblewidget.h"
#include "interface.h"
#include "interfaceadaptor.h"
#include "krunnerapp.h"

//FIXME: 

#define KRUNNER_SIZE 350, 101


// A little hack of a class to let us easily activate a match

class SearchMatch : public QListWidgetItem
{
    public:
        SearchMatch( QAction* action, Plasma::AbstractRunner* runner, QListWidget* parent )
            : QListWidgetItem( parent ),
              m_default( false ),
              m_action( 0 ),
              m_runner( runner )
        {
            setAction( action );
        }

        void activate()
        {
            m_action->activate( QAction::Trigger );
        }

        bool actionEnabled()
        {
            return m_action->isEnabled();
        }

        void setAction( QAction* action )
        {
            m_action = action;
            setIcon( m_action->icon() );
            setText( i18n("%1 (%2)",
                     m_action->text(),
                     m_runner->objectName() ) );

            // in case our new action is now enabled and the old one wasn't, or
            // vice versa
            setDefault( m_default );
        }

        Plasma::AbstractRunner* runner()
        {
            return m_runner;
        }

        void setDefault( bool def ) {
            if ( m_default == def ) {
                return;
            }

            m_default = def;

            if ( m_default ) {
                if ( m_action->isEnabled() ) {
                    setText( text().prepend( i18n("Default: ") ) );
                }
            } else {
                setText( text().mid( 9 ) );
            }
        }

    private:
        bool m_default;
        QAction* m_action;
        Plasma::AbstractRunner* m_runner;
};

GenericItem::GenericItem(Plasma::Svg *renderer, Interface *qgv)
{
    m_background = renderer;
    m_qgv = qgv;
}


void GenericItem::setElement(const QString &element)
{
    m_element = element;
    m_background->resize(350, 101);
}


QRectF GenericItem::itemRectF() const
{
    QRectF rect;
    if (m_element == "top-left" || m_element == "top-right" ||
        m_element == "bottom-left" || m_element == "bottom-right") {

        rect = QRectF(0, 0, 9, 50);

    } else if (m_element == "top" || m_element == "bottom") {

        rect = QRectF(0, 0, 332, 50);

    } else if (m_element == "separator") {
        rect = QRectF(0, 0, 348, 1);
    } else if (m_element == "background") {
        rect = QRectF(0, 0, 350, m_h);
    }
    return rect;
}

QRectF GenericItem::boundingRect() const
{
    if (m_element.isEmpty()) {
        return QRectF(0, 0, 0, 0);
    }
    return itemRectF();
}

void GenericItem::paint(QPainter *painter,
                     const QStyleOptionGraphicsItem *option,
                     QWidget *widget)
{
    m_background->paint(painter, itemRectF(), m_element);
}


// MainItem::MainItem()
// {
//     m_background = new Plasma::Svg( "dialogs/krunner", this );
//     m_background->setContentType(Plasma::Svg::ImageSet);
// //     connect( m_background, SIGNAL(repaintNeeded()), this, SLOT(update()) );
//     m_background->resize(KRUNNER_SIZE);
// }
// 
// void MainItem::paint(QPainter *painter,
//                      const QStyleOptionGraphicsItem *option,
//                      QWidget *widget)
// {
// //     m_background->paint( painter, QRectF(0, 0, 9, 50), "top-left" );
// //     m_background->paint( painter, QRectF(9, 0, 332, 50), "top" );
// //     m_background->paint( painter, QRectF(341, 0, 9, 50), "top-right" );
// //     m_background->paint( painter, QRectF(1, 50, 348, 1), "separator" );
// //     m_background->paint( painter, QRectF(0, 51, 9, 50), "bottom-left" );
// //     m_background->paint( painter, QRectF(9, 51, 332, 50), "bottom" );
// //     m_background->paint( painter, QRectF(341, 51, 9, 50), "bottom-right" );
// //     m_background->paint( painter, 0, 0 );
// }
// 
void PushIcon::setIcon(const QString &icon)
{
    m_icon = icon;
}

void PushIcon::paint(QPainter *painter,
                     const QStyleOptionGraphicsItem *option,
                     QWidget *widget)
{
    KIcon exec(m_icon);
    painter->drawPixmap(0, 0, exec.pixmap(22, 22));
}

Interface::Interface(QWidget* parent)
    : QGraphicsView( parent ),
//       m_expander( 0 ),
      m_defaultMatch( 0 )
{
    m_expanded = false;
    m_background = new Plasma::Svg("dialogs/krunner", this);
    m_background->setContentType(Plasma::Svg::ImageSet);
    m_background->resize(350, 101);
    connect(m_background, SIGNAL(repaintNeeded()), this, SLOT(update()));

    setWindowTitle(i18n("Run Command"));

    connect(&m_searchTimer, SIGNAL(timeout()),
             this, SLOT(fuzzySearch()));

    topleft = new GenericItem(m_background, this);
    topleft->setElement("top-left");
    top = new GenericItem(m_background, this);
    top->setElement("top");
    topright = new GenericItem(m_background, this);
    topright->setElement("top-right");
    separator = new GenericItem(m_background, this);
    separator->setElement("separator");
    separator2 = new GenericItem(m_background, this);
    separator2->setElement("separator");
    background = new GenericItem(m_background, this);
    background->setElement("background");
    bottomleft = new GenericItem(m_background, this);
    bottomleft->setElement("bottom-left");
    bottom = new GenericItem(m_background, this);
    bottom->setElement("bottom");
    bottomright = new GenericItem(m_background, this);
    bottomright->setElement("bottom-right");
    lineEdit = new Plasma::LineEdit;
    m_toggleExpand = new PushIcon(this);
    m_toggleExpand->setIcon("arrow-down");
    close = new PushIcon(this);
    close->setIcon("no");

    QGraphicsScene *scene = new QGraphicsScene(this);
    scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    scene->setSceneRect(0, 0, KRUNNER_SIZE);
    setScene(scene);
    setCacheMode(CacheBackground);
    setRenderHint(QPainter::Antialiasing);


    scene->addItem(background);
    scene->addItem(topleft);
    scene->addItem(top);
    scene->addItem(topright);
    scene->addItem(separator);
    scene->addItem(separator2);
    scene->addItem(bottomleft);
    scene->addItem(bottom);
    scene->addItem(bottomright);
    scene->addItem(lineEdit);
    scene->addItem(m_toggleExpand);
    scene->addItem(close);

    topleft->setPos(0, 0);
    top->setPos(9, 0);
    topright->setPos(341, 0);
    separator->setPos(1, 50);
    separator2->setPos(1, 52);
    separator2->setVisible(false);
    background->setPos(0, 51);
    background->setVisible(false);
//     background->setZValue(0);
    bottomleft->setPos(0, 51);
//     bottomleft->setZValue(135);
    bottom->setPos(9, 51);
//     bottom->setZValue(134);
    bottomright->setPos(341, 51);
//     bottomright->setZValue(133);
    m_toggleExpand->setPos(30, 16);
    lineEdit->setPos(75, 15);
    lineEdit->setZValue(5);
    lineEdit->setTextWidth(200);
    //FIXME: the label doesn't  really behaves correctly (try to write some more text in).
    lineEdit->setDefaultText("Type something here please...");
    close->setPos(305, 16);

    connect(close, SIGNAL(clicked()), this, SLOT(close()));
    connect(m_toggleExpand, SIGNAL(clicked()), this, SLOT(expand()));

    resize(KRUNNER_SIZE);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFrameShape(QFrame::NoFrame);
    new QShortcut(QKeySequence(Qt::Key_Escape), this, SLOT(hide()));
    KDialog::centerOnScreen(this);
}


void Interface::resizeEvent( QResizeEvent *e)
{
//     QGraphicsView::resizeEvent( e );
    int addedHeight = e->size().height()-101;
    QBitmap bitmap(350, 101+addedHeight);
    bitmap.fill(Qt::color0);
    QPainter p(&bitmap);
    p.setBrush(Qt::black);

    // NOTE: this is just for the clipping, so I don't really care if the look of the stuff painted
    // is good or not
    m_background->resize(350, 101);
    m_background->paint( &p, QRectF(0, 0, 9, 50), "top-left" );
    m_background->paint( &p, QRectF(9, 0, 332, 50), "top" );
    m_background->paint( &p, QRectF(341, 0, 9, 50), "top-right" );
    m_background->paint( &p, QRectF(1, 50, 348, 1), "separator" );
    p.drawRect(0, 50, 349, addedHeight);
    m_background->paint( &p, QRectF(0, 51+addedHeight, 9, 50), "bottom-left" );
    m_background->paint( &p, QRectF(9, 51+addedHeight, 332, 50), "bottom" );
    m_background->paint( &p, QRectF(341, 51+addedHeight, 9, 50), "bottom-right" );

    p.end();
    setMask(bitmap);

    topleft->setPos(0, 0);
    top->setPos(9, 0);
    topright->setPos(341, 0);
    m_toggleExpand->setPos(30, 16);
    lineEdit->setPos(75, 15);
    separator->setPos(1, 50);
    if (addedHeight > 0) { //if expanded
        separator->setPos(1, 50);
        background->setPos(0, 51);
        background->setSize(350, addedHeight-1);
        background->setVisible(true);
        separator2->setPos(1, 50+addedHeight);
        separator2->setVisible(true);
    } else {
        background->setVisible(false);
        separator2->setVisible(false);
    }
    bottomleft->setPos(0, 51+addedHeight);
    bottom->setPos(9, 51+addedHeight);
    bottomright->setPos(341, 51+addedHeight);
}

Interface::~Interface()
{
}

void Interface::expand()
{
    disconnect(m_toggleExpand, SIGNAL(clicked()), this, SLOT(expand()));
    connect(m_toggleExpand, SIGNAL(clicked()), this, SLOT(collapse()));
    kDebug() << "expanding..." << endl;
    m_expanded = true;
    QTimeLine *tl = new QTimeLine(200, this);
    tl->setFrameRange(10, 30);
    tl->start();
    connect(tl, SIGNAL(frameChanged(int)), this, SLOT(slotResize(int)));
    m_toggleExpand->setIcon("arrow-up");
}

void Interface::collapse()
{
    disconnect(m_toggleExpand, SIGNAL(clicked()), this, SLOT(collapse()));
    connect(m_toggleExpand, SIGNAL(clicked()), this, SLOT(expand()));
    kDebug() << "collapsing..." << endl;
    m_expanded = false;
    QTimeLine *tl = new QTimeLine(200, this);
    tl->setFrameRange(30, 10);
    tl->start();
    connect(tl, SIGNAL(frameChanged(int)), this, SLOT(slotResize(int)));
    m_toggleExpand->setIcon("arrow-down");
}

void Interface::slotResize(int size)
{
    resize(350, size*101*0.1);
}

void Interface::display( const QString& term)
{
    kDebug() << "display() called, are we visible? " << isVisible() << endl;
//     m_searchTerm->setFocus();

    if ( !term.isEmpty() ) {
//         m_searchTerm->setText( term );
    }

    KWindowSystem::setOnDesktop(winId(), KWindowSystem::currentDesktop());
    KDialog::centerOnScreen( this );
    show();
    KWindowSystem::forceActiveWindow(winId());

    kDebug() << "about to match now that we've shown " << isVisible() << endl;

    match(term);
}

void Interface::switchUser()
{
    Plasma::AbstractRunner *sessionrunner = 0;
    foreach (Plasma::AbstractRunner* runner, m_runners) {
        if (qstrcmp(runner->metaObject()->className(), "SessionRunner") == 0) {
            sessionrunner = runner;
            break;
        }
    }

    if (!sessionrunner) {
        kDebug() << "Could not find the Sessionrunner; not showing any sessions!" << endl;
        return;
    }

    display();
//     m_header->setText(i18n("Switch users"));
//     m_header->setPixmap("user");
    KActionCollection *matches = sessionrunner->matches("SESSIONS", 0, 0);

    foreach (QAction *action, matches->actions()) {
        bool makeDefault = !m_defaultMatch && action->isEnabled();

//         SearchMatch *match = new SearchMatch(action, sessionrunner, m_matchList);
//         m_searchMatches.append(match);

        if (makeDefault) {
//             m_defaultMatch = match;
            m_defaultMatch->setDefault(true);
//             m_runButton->setEnabled(true);
//             m_optionsButton->setEnabled(sessionrunner->hasOptions());
        }
    }

    if (!m_defaultMatch) {
//         m_matchList->addItem(i18n("No desktop sessions available"));
    }
}

void Interface::setWidgetPalettes()
{
    // a nice palette to use with the widgets
    QPalette widgetPalette = palette();
    QColor headerBgColor = widgetPalette.color( QPalette::Active,
                                                QPalette::Base );
    headerBgColor.setAlpha( 200 );
    widgetPalette.setColor( QPalette::Base, headerBgColor );

//     m_header->setPalette( widgetPalette );
//     m_searchTerm->setPalette( widgetPalette );
//     m_matchList->setPalette( widgetPalette );
}

void Interface::resetInterface()
{
//     m_header->setText(i18n("Enter the name of an application, location or search term below."));
//     m_header->setPixmap("system-search");
//     m_searchTerm->clear();
    m_matches.clear();
    m_searchMatches.clear();
//     m_matchList->clear();
//     m_runButton->setEnabled( false );
//     m_optionsButton->setEnabled( false );
    showOptions( false );
}

void Interface::showEvent( QShowEvent* e )
{
    Q_UNUSED( e )

    kDebug() << "show event" << endl;
    QGraphicsView::showEvent( e );
}

void Interface::hideEvent( QHideEvent* e )
{
    kDebug() << "hide event" << endl;
    resetInterface();
    e->accept();
}

void Interface::matchActivated(QListWidgetItem* item)
{
    SearchMatch* match = dynamic_cast<SearchMatch*>(item);
//     m_optionsButton->setEnabled( match && match->runner()->hasOptions() );

    if ( match && match->actionEnabled() ) {
        //kDebug() << "match activated! " << match->text() << endl;
        match->activate();
        hide();
    }
}

void Interface::match(const QString& t)
{
    m_searchTimer.stop();

    m_defaultMatch = 0;
    QString term = t.trimmed();

    if ( term.isEmpty() ) {
        resetInterface();
        return;
    }

    QMap<Plasma::AbstractRunner*, SearchMatch*> matches;

    int matchCount = 0;

    // get the exact matches
    foreach ( Plasma::AbstractRunner* runner, m_runners ) {
        //kDebug() << "\trunner: " << runner->objectName() << endl;
        QAction* exactMatch = runner->exactMatch( term ) ;

        if ( exactMatch ) {
            SearchMatch* match = 0;
            bool makeDefault = !m_defaultMatch && exactMatch->isEnabled();

            QMap<Plasma::AbstractRunner*, SearchMatch*>::iterator it = m_matches.find( runner );
            if ( it != m_matches.end() ) {
                match = it.value();
                match->setAction( exactMatch );
                matches[runner] = match;
                m_matches.erase( it );
            } else {
                match = new SearchMatch( exactMatch, runner, 0 );
//                 m_matchList->insertItem( matchCount, match );
            }

            if ( makeDefault ) {
                match->setDefault( true );
                m_defaultMatch = match;
//                 m_optionsButton->setEnabled( runner->hasOptions() );
//                 m_runButton->setEnabled( true );
            }

            ++matchCount;
            matches[runner] = match;
        }
    }

    if ( !m_defaultMatch ) {
        showOptions( false );
//         m_runButton->setEnabled( false );
    }

    qDeleteAll(m_matches);
    m_matches = matches;
    m_searchTimer.start( 200 );
}

void Interface::fuzzySearch()
{
    m_searchTimer.stop();

    // TODO: we may want to stop this from flickering about as well,
    //       similar to match above
    foreach ( SearchMatch* match, m_searchMatches ) {
        delete match;
    }

    m_searchMatches.clear();

//     QString term = m_searchTerm->text().trimmed();

    // get the inexact matches
//     foreach ( Plasma::AbstractRunner* runner, m_runners ) {
//         KActionCollection* matches = runner->matches( term, 10, 0 );
        //kDebug() << "\t\tturned up " << matches->actions().count() << " matches " << endl;
//         foreach ( QAction* action, matches->actions() ) {
//             bool makeDefault = !m_defaultMatch && action->isEnabled();
            //kDebug() << "\t\t " << action << ": " << action->text() << " " << !m_defaultMatch << " " << action->isEnabled() << endl;
//             SearchMatch* match = new SearchMatch( action, runner, m_matchList );
//             m_searchMatches.append( match );

//             if ( makeDefault ) {
//                 m_defaultMatch = match;
//                 m_defaultMatch->setDefault( true );
//                 m_runButton->setEnabled( true );
//                 m_optionsButton->setEnabled( runner->hasOptions() );
//             }
//         }
//     }
}

void Interface::updateMatches()
{
    //TODO: implement
}

void Interface::exec()
{
//     SearchMatch* match = dynamic_cast<SearchMatch*>( m_matchList->currentItem() );

//     if ( match && match->actionEnabled() ) {
//         matchActivated( match );
//     } else if ( m_defaultMatch ) {
//         matchActivated( m_defaultMatch );
//     }
}

void Interface::showOptions(bool show)
{
    //TODO: in the case where we are no longer showing options
    //      should we have the runner delete it's options?
    if ( show ) {
        if ( !m_defaultMatch || !m_defaultMatch->runner()->hasOptions() ) {
            // in this case, there is nothing to show
            return;
        }

//         if ( !m_expander ) {
            //kDebug() << "creating m_expander" << endl;
//             m_expander = new CollapsibleWidget( this );
//             connect( m_expander, SIGNAL( collapseCompleted() ),
//                      m_expander, SLOT( hide() ) );
//             m_layout->insertWidget( 3, m_expander );
//         }

        //kDebug() << "set inner widget to " << m_defaultMatch->runner()->options() << endl;
//         m_expander->setInnerWidget( m_defaultMatch->runner()->options() );
//         m_expander->show();
//         m_optionsButton->setText( i18n( "Hide Options" ) );
    } else {
//         m_optionsButton->setText( i18n( "Show Options" ) );
//         resize( 400, 250 );
        resize(KRUNNER_SIZE);
    }

//     if ( m_expander ) {
        //TODO: we need to insert an element into the krunner dialog
        //      that is big enough for the options. this will prevent
        //      other items in the dialog from moving around and look
        //      more "natural"; it should appear as if a "drawer" is
        //      being pulled open, e.g. an expander.
//         m_expander->setExpanded( show );
//     }
//     m_optionsButton->setChecked( show );
}

void Interface::setDefaultItem( QListWidgetItem* item )
{
    if ( !item ) {
        return;
    }

    if ( m_defaultMatch ) {
        m_defaultMatch->setDefault( false );
    }

    m_defaultMatch = dynamic_cast<SearchMatch*>( item );

    bool hasOptions = m_defaultMatch && m_defaultMatch->runner()->hasOptions();
//     m_optionsButton->setEnabled( hasOptions );

//     if ( m_expander && !hasOptions ) {
//         m_expander->hide();
//     }
}

#include "interface.moc"
